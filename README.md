### Vixel
Vekwrite + Pixel = Vixel

Vixel is a pixel art font made by Endode for use in the game [Flutterby](https://vekwrite.gitlab.io/projects/endodes_secret_game_project/page.html)
